﻿
namespace GroceryStoreApp
{
    partial class FormSignUpCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSignup = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtPasswordS = new System.Windows.Forms.TextBox();
            this.btnEnterS = new System.Windows.Forms.Button();
            this.txtUsernameS = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSignup
            // 
            this.lblSignup.AutoSize = true;
            this.lblSignup.Font = new System.Drawing.Font("Verdana", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSignup.ForeColor = System.Drawing.Color.White;
            this.lblSignup.Location = new System.Drawing.Point(250, 99);
            this.lblSignup.Name = "lblSignup";
            this.lblSignup.Size = new System.Drawing.Size(164, 45);
            this.lblSignup.TabIndex = 11;
            this.lblSignup.Text = "Sign Up";
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtConfirmPassword.Location = new System.Drawing.Point(205, 344);
            this.txtConfirmPassword.Multiline = true;
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.Size = new System.Drawing.Size(264, 49);
            this.txtConfirmPassword.TabIndex = 10;
            // 
            // txtPasswordS
            // 
            this.txtPasswordS.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtPasswordS.Location = new System.Drawing.Point(205, 278);
            this.txtPasswordS.Multiline = true;
            this.txtPasswordS.Name = "txtPasswordS";
            this.txtPasswordS.Size = new System.Drawing.Size(264, 49);
            this.txtPasswordS.TabIndex = 9;
            // 
            // btnEnterS
            // 
            this.btnEnterS.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEnterS.Location = new System.Drawing.Point(205, 414);
            this.btnEnterS.Name = "btnEnterS";
            this.btnEnterS.Size = new System.Drawing.Size(264, 39);
            this.btnEnterS.TabIndex = 8;
            this.btnEnterS.Text = "Enter";
            this.btnEnterS.UseVisualStyleBackColor = true;
            this.btnEnterS.Click += new System.EventHandler(this.btnEnterS_Click);
            // 
            // txtUsernameS
            // 
            this.txtUsernameS.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtUsernameS.Location = new System.Drawing.Point(205, 214);
            this.txtUsernameS.Multiline = true;
            this.txtUsernameS.Name = "txtUsernameS";
            this.txtUsernameS.Size = new System.Drawing.Size(264, 49);
            this.txtUsernameS.TabIndex = 7;
            // 
            // FormSignUpCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateBlue;
            this.ClientSize = new System.Drawing.Size(683, 589);
            this.Controls.Add(this.lblSignup);
            this.Controls.Add(this.txtConfirmPassword);
            this.Controls.Add(this.txtPasswordS);
            this.Controls.Add(this.btnEnterS);
            this.Controls.Add(this.txtUsernameS);
            this.Name = "FormSignUpCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sign Up for Customers";
            this.Load += new System.EventHandler(this.FormSignUpCustomer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSignup;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtPasswordS;
        private System.Windows.Forms.Button btnEnterS;
        private System.Windows.Forms.TextBox txtUsernameS;
    }
}