﻿//SIGN UP FOR CUSTOMER
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryStoreApp
{
    public partial class FormSignUpCustomer : Form
    {
        public FormSignUpCustomer()
        {
            InitializeComponent();
            txtUsernameS.ForeColor = SystemColors.GrayText;
            txtPasswordS.ForeColor = SystemColors.GrayText;
            txtConfirmPassword.ForeColor = SystemColors.GrayText;
            txtUsernameS.Text = "Please Enter Your Name";
            txtPasswordS.Text = "Please Enter Your Password";
            txtConfirmPassword.Text = "Please Confirm Your Password";

            this.txtUsernameS.Leave += new System.EventHandler(this.txtUsernameS_Leave);
            this.txtUsernameS.Enter += new System.EventHandler(this.txtUsernameS_Enter);
            this.txtPasswordS.Leave += new System.EventHandler(this.txtPasswordS_Leave);
            this.txtPasswordS.Enter += new System.EventHandler(this.txtPasswordS_Enter);
            this.txtConfirmPassword.Leave += new System.EventHandler(this.txtConfirmPassword_Leave);
            this.txtConfirmPassword.Enter += new System.EventHandler(this.txtConfirmPassword_Enter);
        }

        private void btnEnterS_Click(object sender, EventArgs e)
        {
            FormLoginCustomer flc = new FormLoginCustomer();
            flc.Show();
            this.Hide();
        }

        private void FormSignUpCustomer_Load(object sender, EventArgs e)
        {

        }
        private void txtUsernameS_Enter(object sender, EventArgs e)
        {

            if (txtUsernameS.Text == "Please Enter Your Name")
            {
                txtUsernameS.Text = "";
                txtUsernameS.ForeColor = SystemColors.WindowText;
            }
        }

        private void txtUsernameS_Leave(object sender, EventArgs e)
        {
            if (txtUsernameS.Text.Length == 0)
            {
                txtUsernameS.Text = "Please Enter Your Name";
                txtUsernameS.ForeColor = SystemColors.GrayText;
            }
        }
        private void txtPasswordS_Enter(object sender, EventArgs e)
        {
            if (txtPasswordS.Text == "Please Enter Your Password")
            {
                txtPasswordS.Text = "";
                txtPasswordS.ForeColor = SystemColors.WindowText;
            }
        }

        private void txtPasswordS_Leave(object sender, EventArgs e)
        {
            if (txtPasswordS.Text.Length == 0)
            {
                txtPasswordS.Text = "Please Enter Your Password";
                txtPasswordS.ForeColor = SystemColors.GrayText;
            }
        }
        private void txtConfirmPassword_Enter(object sender, EventArgs e)
        {
            if (txtConfirmPassword.Text == "Please Confirm Your Password")
            {
                txtConfirmPassword.Text = "";
                txtConfirmPassword.ForeColor = SystemColors.WindowText;
            }
        }

        private void txtConfirmPassword_Leave(object sender, EventArgs e)
        {
            if (txtConfirmPassword.Text.Length == 0)
            {
                txtConfirmPassword.Text = "Please Confirm Your Password";
                txtConfirmPassword.ForeColor = SystemColors.GrayText;
            }
        }
    }
}
