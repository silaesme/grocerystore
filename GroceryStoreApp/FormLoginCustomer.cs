﻿//LOGIN FOR CUSTOMER
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;


namespace GroceryStoreApp
{
    public partial class FormLoginCustomer : Form
    {
        SqlCommand Command;
        static int ID = 4;
        public int MyID
        {
            get => ID;
            set => ID = value;
        }
        public FormLoginCustomer()
        {
            InitializeComponent();
        }

        private void FormLoginCustomer_Load(object sender, EventArgs e)
        {
        }

        private void exit_picturebox_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            bool flag = false;
            HashCryptology hashCriptology = new HashCryptology();
            string sifre = txtPassword.Text;
            hashCriptology.MD5Sifrele(ref sifre);
            Database database = Database.CreateSingle();
            database.GetConnection();

            SqlCommand command = new SqlCommand("SELECT * FROM CUSTOMER", Database.CreateSingle().Sqlconnection);
            Database.CreateSingle().Sqlconnection.Open();
            SqlDataReader dr = command.ExecuteReader();


            while (dr.Read())
            {

                if (dr.GetString(5) == txtUsername.Text)
                {

                    if (dr.GetString(4) == sifre)
                    {
                        CustomerUser loginedCustomer = new CustomerUser();

                        loginedCustomer.Name = dr.GetString(1);
                        loginedCustomer.Telephone = dr.GetString(2);
                        loginedCustomer.Address = dr.GetString(3);
                        loginedCustomer.Password = dr.GetString(4);
                        loginedCustomer.Username = dr.GetString(5);
                        //loginedCustomer.Path = dr.GetString(6);

                        LoginedCustomer.Instance().CustomerUser = loginedCustomer;

                        FormGroceryStoreCustomerPanel customerPanel = new();
                        customerPanel.Show();
                        this.Hide();
                        flag = true;
                        break;
                    }
                    else
                    {
                        flag = true;
                        MessageBox.Show("Your password or username is not correct. Please check your informations.", "Opppppps");
                        break;
                    }
                }
            }

            if (flag == false)
                MessageBox.Show("Your password or username is not correct. Please check your informations.", "Opppppps");

            Database.CreateSingle().Sqlconnection.Close();
        }

        private void sign_up_Click(object sender, EventArgs e)
        {
            bool flag = false;
            HashCryptology hashCriptology = new HashCryptology();
            string sifre = txtPasswordS.Text;
            hashCriptology.MD5Sifrele(ref sifre);
            Database database = Database.CreateSingle();
            database.GetConnection();

            SqlCommand command2 = new SqlCommand("SELECT Username FROM CUSTOMER", Database.CreateSingle().Sqlconnection);
            Database.CreateSingle().Sqlconnection.Open();
            SqlDataReader dr = command2.ExecuteReader();

            if (txtPasswordS.Text != "" && txtUsernameS.Text != "" && txtAdres.Text != "" &&
                txtName.Text != "")
            {
                if (txtPasswordS.Text == txtConfirmPassword.Text)
                {
                    string command = "INSERT INTO Customer(ID,_Name,Phone,Address, Password, Username,Path) VALUES(@id,@_Name,@Phone,@Address,@Password,@Username,@path)";
                    Command = new SqlCommand(command, Database.CreateSingle().Sqlconnection);
                    Command.Parameters.AddWithValue("@id", ID);
                    Command.Parameters.AddWithValue("@_Name", txtName.Text);
                    Command.Parameters.AddWithValue("@Phone", txtTelefon.Text);
                    Command.Parameters.AddWithValue("@Address", txtAdres.Text);
                    Command.Parameters.AddWithValue("@Password", sifre);
                    Command.Parameters.AddWithValue("@Username", txtUsernameS.Text);
                    Command.Parameters.AddWithValue("@path", "default-picture.jpg");

                    while (dr.Read())
                    {
                        if (dr.GetString(0) == txtUsernameS.Text)
                        {
                            MessageBox.Show("Your username is already taken!", "Invalid");
                            flag = true;
                            break;
                        }
                    }
                    Database.CreateSingle().Sqlconnection.Close();
                    if (flag == false)
                    {
                        Database.CreateSingle().Sqlconnection.Open();
                        Command.ExecuteNonQuery();
                        Database.CreateSingle().Sqlconnection.Close();
                        ID++;
                        labelSuccess.Text = "Your account has been created succesfully!";
                    }
                }
                else
                {
                    MessageBox.Show("Your password and confirm password doesn't match!", "Invalid");
                }
            }
            else
            {
                MessageBox.Show("Please fill all the blanks!", "Invalid");
            }
            //    Database database=Database.CreateSingle();
            //    database.GetConnection();
            //    FormGroceryStoreAdminPanel formGroceryStoreAdminPanel = new FormGroceryStoreAdminPanel();
            //    if (txtPasswordS.Text == txtConfirmPassword.Text)
            //    {
            //        string command = "INSERT INTO Customer(ID,_Name,Phone,Address, Password, Username,Path) VALUES(@id,@_Name,@Phone,@Address,@Password,@Username,@path)";
            //        Command = new SqlCommand(command, Database.CreateSingle().Sqlconnection);
            //        Command.Parameters.AddWithValue("@id", ID);
            //        Command.Parameters.AddWithValue("@_Name", txtName.Text);
            //        Command.Parameters.AddWithValue("@Phone", txtTelefon.Text);
            //        Command.Parameters.AddWithValue("@Address", txtAdres.Text);
            //        Command.Parameters.AddWithValue("@Password", txtPasswordS.Text);
            //        Command.Parameters.AddWithValue("@Username", txtUsernameS.Text);
            //        Command.Parameters.AddWithValue("@path", "default-picture.jpg");

            //        Database.CreateSingle().Sqlconnection.Open();
            //        Command.ExecuteNonQuery();
            //        Database.CreateSingle().Sqlconnection.Close();
            //        ID++;
            //        labelSuccess.Text = "Your account has been created succesfully!";
            //    }
            //    else
            //    {
            //        MessageBox.Show("Your password and confirm password doesn't match!", "Opppppps");
            //    }
            //}
        }

      
    }
}