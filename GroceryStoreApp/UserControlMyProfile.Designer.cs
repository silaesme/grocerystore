﻿
namespace GroceryStoreApp
{
    partial class UserControlMyProfile
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxCustomer = new System.Windows.Forms.PictureBox();
            this.txtChangePassword = new System.Windows.Forms.TextBox();
            this.txtChangePhoneNumber = new System.Windows.Forms.TextBox();
            this.txtChangeAddress = new System.Windows.Forms.TextBox();
            this.txtChangeName = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.btnChangePhoto = new System.Windows.Forms.Button();
            this.btnDeleteAccount = new System.Windows.Forms.Button();
            this.lblUsername = new System.Windows.Forms.Label();
            this.btnUpdateInfo = new System.Windows.Forms.Button();
            this.lblInfoUpdate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCustomer
            // 
            this.pictureBoxCustomer.Location = new System.Drawing.Point(747, 127);
            this.pictureBoxCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBoxCustomer.Name = "pictureBoxCustomer";
            this.pictureBoxCustomer.Size = new System.Drawing.Size(189, 212);
            this.pictureBoxCustomer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCustomer.TabIndex = 15;
            this.pictureBoxCustomer.TabStop = false;
            // 
            // txtChangePassword
            // 
            this.txtChangePassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChangePassword.Location = new System.Drawing.Point(251, 379);
            this.txtChangePassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChangePassword.Name = "txtChangePassword";
            this.txtChangePassword.PasswordChar = '*';
            this.txtChangePassword.Size = new System.Drawing.Size(173, 27);
            this.txtChangePassword.TabIndex = 16;
            // 
            // txtChangePhoneNumber
            // 
            this.txtChangePhoneNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChangePhoneNumber.Location = new System.Drawing.Point(251, 213);
            this.txtChangePhoneNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChangePhoneNumber.Name = "txtChangePhoneNumber";
            this.txtChangePhoneNumber.Size = new System.Drawing.Size(173, 27);
            this.txtChangePhoneNumber.TabIndex = 18;
            // 
            // txtChangeAddress
            // 
            this.txtChangeAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChangeAddress.Location = new System.Drawing.Point(251, 297);
            this.txtChangeAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChangeAddress.Name = "txtChangeAddress";
            this.txtChangeAddress.Size = new System.Drawing.Size(173, 27);
            this.txtChangeAddress.TabIndex = 19;
            // 
            // txtChangeName
            // 
            this.txtChangeName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChangeName.Location = new System.Drawing.Point(251, 127);
            this.txtChangeName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtChangeName.Name = "txtChangeName";
            this.txtChangeName.Size = new System.Drawing.Size(173, 27);
            this.txtChangeName.TabIndex = 20;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(175, 386);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(70, 20);
            this.lblPassword.TabIndex = 22;
            this.lblPassword.Text = "Password";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(195, 220);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(50, 20);
            this.lblPhoneNumber.TabIndex = 23;
            this.lblPhoneNumber.Text = "Phone";
            this.lblPhoneNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(183, 304);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(62, 20);
            this.lblAddress.TabIndex = 24;
            this.lblAddress.Text = "Address";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(196, 134);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 20);
            this.lblName.TabIndex = 25;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnChangePhoto
            // 
            this.btnChangePhoto.BackColor = System.Drawing.Color.BurlyWood;
            this.btnChangePhoto.FlatAppearance.BorderSize = 0;
            this.btnChangePhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangePhoto.Location = new System.Drawing.Point(747, 370);
            this.btnChangePhoto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnChangePhoto.Name = "btnChangePhoto";
            this.btnChangePhoto.Size = new System.Drawing.Size(189, 48);
            this.btnChangePhoto.TabIndex = 26;
            this.btnChangePhoto.Text = "Add or Change Photo";
            this.btnChangePhoto.UseVisualStyleBackColor = false;
            this.btnChangePhoto.Click += new System.EventHandler(this.btnChangePhoto_Click);
            // 
            // btnDeleteAccount
            // 
            this.btnDeleteAccount.BackColor = System.Drawing.Color.BurlyWood;
            this.btnDeleteAccount.FlatAppearance.BorderSize = 0;
            this.btnDeleteAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteAccount.ForeColor = System.Drawing.Color.Red;
            this.btnDeleteAccount.Location = new System.Drawing.Point(747, 453);
            this.btnDeleteAccount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteAccount.Name = "btnDeleteAccount";
            this.btnDeleteAccount.Size = new System.Drawing.Size(185, 48);
            this.btnDeleteAccount.TabIndex = 27;
            this.btnDeleteAccount.Text = "Delete Account";
            this.btnDeleteAccount.UseVisualStyleBackColor = false;
            this.btnDeleteAccount.Click += new System.EventHandler(this.btnDeleteAccount_Click);
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblUsername.Location = new System.Drawing.Point(293, 57);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(89, 20);
            this.lblUsername.TabIndex = 28;
            this.lblUsername.Text = "USERNAME";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUpdateInfo
            // 
            this.btnUpdateInfo.BackColor = System.Drawing.Color.BurlyWood;
            this.btnUpdateInfo.FlatAppearance.BorderSize = 0;
            this.btnUpdateInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateInfo.Location = new System.Drawing.Point(243, 453);
            this.btnUpdateInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpdateInfo.Name = "btnUpdateInfo";
            this.btnUpdateInfo.Size = new System.Drawing.Size(189, 48);
            this.btnUpdateInfo.TabIndex = 29;
            this.btnUpdateInfo.Text = "Update Info";
            this.btnUpdateInfo.UseVisualStyleBackColor = false;
            this.btnUpdateInfo.Click += new System.EventHandler(this.btnUpdateInfo_Click);
            // 
            // lblInfoUpdate
            // 
            this.lblInfoUpdate.Location = new System.Drawing.Point(488, 453);
            this.lblInfoUpdate.Name = "lblInfoUpdate";
            this.lblInfoUpdate.Size = new System.Drawing.Size(177, 48);
            this.lblInfoUpdate.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(473, 334);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 101);
            this.label1.TabIndex = 31;
            this.label1.Text = "If you want to change your password, please enter the text. Otherwise enter your " +
    "current password.\r\n\r\n";
            // 
            // UserControlMyProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblInfoUpdate);
            this.Controls.Add(this.btnUpdateInfo);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.btnDeleteAccount);
            this.Controls.Add(this.btnChangePhoto);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lblPhoneNumber);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtChangeName);
            this.Controls.Add(this.txtChangeAddress);
            this.Controls.Add(this.txtChangePhoneNumber);
            this.Controls.Add(this.txtChangePassword);
            this.Controls.Add(this.pictureBoxCustomer);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UserControlMyProfile";
            this.Size = new System.Drawing.Size(1110, 559);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxCustomer;
        private System.Windows.Forms.TextBox txtChangePassword;
        private System.Windows.Forms.TextBox txtChangePhoneNumber;
        private System.Windows.Forms.TextBox txtChangeAddress;
        private System.Windows.Forms.TextBox txtChangeName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnChangePhoto;
        private System.Windows.Forms.Button btnDeleteAccount;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Button btnUpdateInfo;
        private System.Windows.Forms.Label lblInfoUpdate;
        private System.Windows.Forms.Label label1;
    }
}
