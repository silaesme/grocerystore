﻿
namespace GroceryStoreApp
{
    partial class FormLoginCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnter = new System.Windows.Forms.Button();
            this.lblLogin = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtPasswordS = new System.Windows.Forms.TextBox();
            this.sign_up = new System.Windows.Forms.Button();
            this.txtUsernameS = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.exit_picturebox = new System.Windows.Forms.PictureBox();
            this.labelSuccess = new System.Windows.Forms.Label();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.txtAdres = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblNameH = new System.Windows.Forms.Label();
            this.lblAdresH = new System.Windows.Forms.Label();
            this.lblTelefonH = new System.Windows.Forms.Label();
            this.lblUsernameHSign = new System.Windows.Forms.Label();
            this.lblPasswordHSign = new System.Windows.Forms.Label();
            this.lblConfirmPasswordSign = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.exit_picturebox)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEnter
            // 
            this.btnEnter.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnEnter.FlatAppearance.BorderSize = 0;
            this.btnEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnter.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnEnter.ForeColor = System.Drawing.Color.White;
            this.btnEnter.Location = new System.Drawing.Point(27, 264);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(259, 39);
            this.btnEnter.TabIndex = 9;
            this.btnEnter.Text = "Login";
            this.btnEnter.UseVisualStyleBackColor = false;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Font = new System.Drawing.Font("Verdana", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLogin.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblLogin.Location = new System.Drawing.Point(97, 20);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(119, 45);
            this.lblLogin.TabIndex = 8;
            this.lblLogin.Text = "Login";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtPassword.Location = new System.Drawing.Point(27, 194);
            this.txtPassword.Multiline = true;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(259, 37);
            this.txtPassword.TabIndex = 7;
            // 
            // txtUsername
            // 
            this.txtUsername.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtUsername.Location = new System.Drawing.Point(27, 132);
            this.txtUsername.Multiline = true;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(259, 37);
            this.txtUsername.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label2.Location = new System.Drawing.Point(257, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 45);
            this.label2.TabIndex = 16;
            this.label2.Text = "Sign Up";
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtConfirmPassword.Location = new System.Drawing.Point(358, 243);
            this.txtConfirmPassword.Multiline = true;
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(265, 37);
            this.txtConfirmPassword.TabIndex = 15;
            // 
            // txtPasswordS
            // 
            this.txtPasswordS.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtPasswordS.Location = new System.Drawing.Point(358, 179);
            this.txtPasswordS.Multiline = true;
            this.txtPasswordS.Name = "txtPasswordS";
            this.txtPasswordS.PasswordChar = '*';
            this.txtPasswordS.Size = new System.Drawing.Size(265, 37);
            this.txtPasswordS.TabIndex = 14;
            // 
            // sign_up
            // 
            this.sign_up.BackColor = System.Drawing.Color.DarkSlateGray;
            this.sign_up.FlatAppearance.BorderSize = 0;
            this.sign_up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sign_up.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.sign_up.ForeColor = System.Drawing.Color.White;
            this.sign_up.Location = new System.Drawing.Point(207, 315);
            this.sign_up.Name = "sign_up";
            this.sign_up.Size = new System.Drawing.Size(264, 39);
            this.sign_up.TabIndex = 13;
            this.sign_up.Text = "Sign Up";
            this.sign_up.UseVisualStyleBackColor = false;
            this.sign_up.Click += new System.EventHandler(this.sign_up_Click);
            // 
            // txtUsernameS
            // 
            this.txtUsernameS.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtUsernameS.Location = new System.Drawing.Point(358, 113);
            this.txtUsernameS.Multiline = true;
            this.txtUsernameS.Name = "txtUsernameS";
            this.txtUsernameS.Size = new System.Drawing.Size(265, 37);
            this.txtUsernameS.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.label1.Location = new System.Drawing.Point(60, 330);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 72);
            this.label1.TabIndex = 17;
            this.label1.Text = "Create an account if you are not signed up";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // exit_picturebox
            // 
            this.exit_picturebox.Image = global::GroceryStoreApp.Properties.Resources.exit;
            this.exit_picturebox.Location = new System.Drawing.Point(1091, 12);
            this.exit_picturebox.Name = "exit_picturebox";
            this.exit_picturebox.Size = new System.Drawing.Size(38, 32);
            this.exit_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exit_picturebox.TabIndex = 20;
            this.exit_picturebox.TabStop = false;
            this.exit_picturebox.Click += new System.EventHandler(this.exit_picturebox_Click);
            // 
            // labelSuccess
            // 
            this.labelSuccess.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelSuccess.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.labelSuccess.Location = new System.Drawing.Point(219, 357);
            this.labelSuccess.Name = "labelSuccess";
            this.labelSuccess.Size = new System.Drawing.Size(239, 64);
            this.labelSuccess.TabIndex = 23;
            this.labelSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTelefon
            // 
            this.txtTelefon.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtTelefon.Location = new System.Drawing.Point(55, 243);
            this.txtTelefon.Multiline = true;
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(265, 37);
            this.txtTelefon.TabIndex = 24;
            // 
            // txtAdres
            // 
            this.txtAdres.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtAdres.Location = new System.Drawing.Point(55, 178);
            this.txtAdres.Multiline = true;
            this.txtAdres.Name = "txtAdres";
            this.txtAdres.Size = new System.Drawing.Size(265, 37);
            this.txtAdres.TabIndex = 25;
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Malgun Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtName.Location = new System.Drawing.Point(55, 113);
            this.txtName.Multiline = true;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(265, 37);
            this.txtName.TabIndex = 26;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtUsername);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.lblLogin);
            this.panel1.Controls.Add(this.btnEnter);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(59, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 443);
            this.panel1.TabIndex = 27;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblConfirmPasswordSign);
            this.panel2.Controls.Add(this.lblPasswordHSign);
            this.panel2.Controls.Add(this.lblUsernameHSign);
            this.panel2.Controls.Add(this.lblTelefonH);
            this.panel2.Controls.Add(this.lblAdresH);
            this.panel2.Controls.Add(this.lblNameH);
            this.panel2.Controls.Add(this.txtName);
            this.panel2.Controls.Add(this.txtTelefon);
            this.panel2.Controls.Add(this.labelSuccess);
            this.panel2.Controls.Add(this.txtAdres);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.sign_up);
            this.panel2.Controls.Add(this.txtUsernameS);
            this.panel2.Controls.Add(this.txtConfirmPassword);
            this.panel2.Controls.Add(this.txtPasswordS);
            this.panel2.Location = new System.Drawing.Point(401, 95);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(681, 443);
            this.panel2.TabIndex = 28;
            // 
            // lblNameH
            // 
            this.lblNameH.AutoSize = true;
            this.lblNameH.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNameH.Location = new System.Drawing.Point(55, 93);
            this.lblNameH.Name = "lblNameH";
            this.lblNameH.Size = new System.Drawing.Size(46, 17);
            this.lblNameH.TabIndex = 27;
            this.lblNameH.Text = "Name:";
            // 
            // lblAdresH
            // 
            this.lblAdresH.AutoSize = true;
            this.lblAdresH.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAdresH.Location = new System.Drawing.Point(55, 158);
            this.lblAdresH.Name = "lblAdresH";
            this.lblAdresH.Size = new System.Drawing.Size(60, 17);
            this.lblAdresH.TabIndex = 28;
            this.lblAdresH.Text = "Address:";
            // 
            // lblTelefonH
            // 
            this.lblTelefonH.AutoSize = true;
            this.lblTelefonH.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblTelefonH.Location = new System.Drawing.Point(55, 223);
            this.lblTelefonH.Name = "lblTelefonH";
            this.lblTelefonH.Size = new System.Drawing.Size(103, 17);
            this.lblTelefonH.TabIndex = 29;
            this.lblTelefonH.Text = "Phone Number:";
            // 
            // lblUsernameHSign
            // 
            this.lblUsernameHSign.AutoSize = true;
            this.lblUsernameHSign.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblUsernameHSign.Location = new System.Drawing.Point(358, 93);
            this.lblUsernameHSign.Name = "lblUsernameHSign";
            this.lblUsernameHSign.Size = new System.Drawing.Size(71, 17);
            this.lblUsernameHSign.TabIndex = 30;
            this.lblUsernameHSign.Text = "Username:";
            // 
            // lblPasswordHSign
            // 
            this.lblPasswordHSign.AutoSize = true;
            this.lblPasswordHSign.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPasswordHSign.Location = new System.Drawing.Point(358, 158);
            this.lblPasswordHSign.Name = "lblPasswordHSign";
            this.lblPasswordHSign.Size = new System.Drawing.Size(68, 17);
            this.lblPasswordHSign.TabIndex = 31;
            this.lblPasswordHSign.Text = "Password:";
            // 
            // lblConfirmPasswordSign
            // 
            this.lblConfirmPasswordSign.AutoSize = true;
            this.lblConfirmPasswordSign.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblConfirmPasswordSign.Location = new System.Drawing.Point(358, 223);
            this.lblConfirmPasswordSign.Name = "lblConfirmPasswordSign";
            this.lblConfirmPasswordSign.Size = new System.Drawing.Size(120, 17);
            this.lblConfirmPasswordSign.TabIndex = 32;
            this.lblConfirmPasswordSign.Text = "Confirm Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(27, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "Username:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Malgun Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(27, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 32;
            this.label4.Text = "Password:";
            // 
            // FormLoginCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1141, 633);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.exit_picturebox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormLoginCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login for Customers";
            this.Load += new System.EventHandler(this.FormLoginCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.exit_picturebox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtPasswordS;
        private System.Windows.Forms.Button sign_up;
        private System.Windows.Forms.TextBox txtUsernameS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox exit_picturebox;
        private System.Windows.Forms.Label labelSuccess;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.TextBox txtAdres;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblConfirmPasswordSign;
        private System.Windows.Forms.Label lblPasswordHSign;
        private System.Windows.Forms.Label lblUsernameHSign;
        private System.Windows.Forms.Label lblTelefonH;
        private System.Windows.Forms.Label lblAdresH;
        private System.Windows.Forms.Label lblNameH;
    }
}