﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace GroceryStoreApp
{
    public partial class UserControlProductPanel : UserControl
    {
        private string desc = "";

        public UserControlProductPanel(string name, string price)
        {
            InitializeComponent();

            lblName.Text = name;
            lblPrice.Text = price;
            Database database = Database.CreateSingle();
            database.GetConnection();

            string imagename = "";
            var dirs = Directory.GetFiles(@"Images", "*.*").Where(s => s.EndsWith(".png") || s.EndsWith(".jpg") || s.EndsWith(".jpeg"));
            SqlCommand command2 = new SqlCommand("SELECT Path FROM dbo.Products WHERE name = @name", Database.CreateSingle().Sqlconnection);
            command2.Parameters.AddWithValue("@name", lblName.Text);
            Database.CreateSingle().Sqlconnection.Open();
            SqlDataReader dr2 = command2.ExecuteReader();

            while (dr2.Read())
            {
                imagename = dr2.GetString(0);
            }

            Database.CreateSingle().Sqlconnection.Close();

            foreach (var item in dirs)
            {
                if (Path.GetFileName(item) == imagename)
                {
                    pictureBoxItem.Image = new Bitmap(item);
                }
            }
        }

        private void btnReadMore_Click(object sender, EventArgs e)
        {

            Database database = Database.CreateSingle();
            database.GetConnection();
            // , Description = @Description, Price = @Price
            SqlCommand command = new SqlCommand("SELECT * FROM Products WHERE Name = @Name", Database.CreateSingle().Sqlconnection);
            command.Parameters.AddWithValue("@Name", lblName.Text);

            Database.CreateSingle().Sqlconnection.Open();
            SqlDataReader dr = command.ExecuteReader();

            foreach (var item in FormGroceryStoreCustomerPanel.Instance.flowPanelMain.Controls.OfType<UserControlProductPanel>())
            {
                item.Visible = false;
            }
            if (!FormGroceryStoreCustomerPanel.Instance.flowPanelMain.Controls.ContainsKey("UserControlProductInformations"))
            {
                while (dr.Read())
                {
                    UserControlProductInformations productInformations = new();
                    FormGroceryStoreCustomerPanel.Instance.flowPanelMain.Controls.Add(productInformations);
                    productInformations.WriteDown(dr.GetString(0), dr.GetString(1), dr.GetString(2));
                    //productInformations.Dock = DockStyle.Fill;
                }
                
            }
            else
            {
                var productInformations = FormGroceryStoreCustomerPanel.Instance.flowPanelMain.Controls.OfType<UserControlProductInformations>().First();
                while(dr.Read())
                    productInformations.WriteDown(dr.GetString(0), dr.GetString(1), dr.GetString(2));
            }

            FormGroceryStoreCustomerPanel.Instance.flowPanelMain.Controls["UserControlProductInformations"].BringToFront();
            FormGroceryStoreCustomerPanel.Instance.flowPanelMain.Controls["UserControlProductInformations"].Visible = true;
            Database.CreateSingle().Sqlconnection.Close();
           
        }
    }
}