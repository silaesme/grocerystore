﻿
namespace GroceryStoreApp
{
    partial class UserControlProductInformations
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxItem = new System.Windows.Forms.PictureBox();
            this.lblPriceH = new System.Windows.Forms.Label();
            this.lblNameH = new System.Windows.Forms.Label();
            this.lblDescriptionH = new System.Windows.Forms.Label();
            this.btnAddToCart = new System.Windows.Forms.Button();
            this.lblPriceinfo = new System.Windows.Forms.Label();
            this.lblNameinfo = new System.Windows.Forms.Label();
            this.lblDescriptioninfo = new System.Windows.Forms.Label();
            this.pictureBoxReturn = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxReturn)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxItem
            // 
            this.pictureBoxItem.Location = new System.Drawing.Point(171, 118);
            this.pictureBoxItem.Name = "pictureBoxItem";
            this.pictureBoxItem.Size = new System.Drawing.Size(312, 308);
            this.pictureBoxItem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxItem.TabIndex = 5;
            this.pictureBoxItem.TabStop = false;
            // 
            // lblPriceH
            // 
            this.lblPriceH.AutoSize = true;
            this.lblPriceH.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblPriceH.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblPriceH.Location = new System.Drawing.Point(30, 83);
            this.lblPriceH.Name = "lblPriceH";
            this.lblPriceH.Size = new System.Drawing.Size(54, 23);
            this.lblPriceH.TabIndex = 6;
            this.lblPriceH.Text = "Price:";
            // 
            // lblNameH
            // 
            this.lblNameH.AutoSize = true;
            this.lblNameH.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblNameH.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblNameH.Location = new System.Drawing.Point(30, 29);
            this.lblNameH.Name = "lblNameH";
            this.lblNameH.Size = new System.Drawing.Size(62, 23);
            this.lblNameH.TabIndex = 7;
            this.lblNameH.Text = "Name:";
            // 
            // lblDescriptionH
            // 
            this.lblDescriptionH.AutoSize = true;
            this.lblDescriptionH.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblDescriptionH.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblDescriptionH.Location = new System.Drawing.Point(30, 138);
            this.lblDescriptionH.Name = "lblDescriptionH";
            this.lblDescriptionH.Size = new System.Drawing.Size(107, 23);
            this.lblDescriptionH.TabIndex = 8;
            this.lblDescriptionH.Text = "Description:";
            // 
            // btnAddToCart
            // 
            this.btnAddToCart.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnAddToCart.FlatAppearance.BorderSize = 0;
            this.btnAddToCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddToCart.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAddToCart.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnAddToCart.Location = new System.Drawing.Point(91, 230);
            this.btnAddToCart.Name = "btnAddToCart";
            this.btnAddToCart.Size = new System.Drawing.Size(119, 39);
            this.btnAddToCart.TabIndex = 9;
            this.btnAddToCart.Text = "Add To Cart";
            this.btnAddToCart.UseVisualStyleBackColor = false;
            this.btnAddToCart.Click += new System.EventHandler(this.btnAddToCart_Click);
            // 
            // lblPriceinfo
            // 
            this.lblPriceinfo.AutoSize = true;
            this.lblPriceinfo.Location = new System.Drawing.Point(172, 86);
            this.lblPriceinfo.Name = "lblPriceinfo";
            this.lblPriceinfo.Size = new System.Drawing.Size(44, 20);
            this.lblPriceinfo.TabIndex = 10;
            this.lblPriceinfo.Text = "99.99";
            // 
            // lblNameinfo
            // 
            this.lblNameinfo.AutoSize = true;
            this.lblNameinfo.Location = new System.Drawing.Point(172, 32);
            this.lblNameinfo.Name = "lblNameinfo";
            this.lblNameinfo.Size = new System.Drawing.Size(50, 20);
            this.lblNameinfo.TabIndex = 11;
            this.lblNameinfo.Text = "label2";
            // 
            // lblDescriptioninfo
            // 
            this.lblDescriptioninfo.Location = new System.Drawing.Point(172, 140);
            this.lblDescriptioninfo.Name = "lblDescriptioninfo";
            this.lblDescriptioninfo.Size = new System.Drawing.Size(112, 87);
            this.lblDescriptioninfo.TabIndex = 12;
            this.lblDescriptioninfo.Text = "label3";
            // 
            // pictureBoxReturn
            // 
            this.pictureBoxReturn.Image = global::GroceryStoreApp.Properties.Resources.back;
            this.pictureBoxReturn.Location = new System.Drawing.Point(32, 35);
            this.pictureBoxReturn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBoxReturn.Name = "pictureBoxReturn";
            this.pictureBoxReturn.Size = new System.Drawing.Size(40, 45);
            this.pictureBoxReturn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxReturn.TabIndex = 13;
            this.pictureBoxReturn.TabStop = false;
            this.pictureBoxReturn.Click += new System.EventHandler(this.pictureBoxReturn_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblDescriptionH);
            this.panel1.Controls.Add(this.lblPriceH);
            this.panel1.Controls.Add(this.lblDescriptioninfo);
            this.panel1.Controls.Add(this.lblNameH);
            this.panel1.Controls.Add(this.lblNameinfo);
            this.panel1.Controls.Add(this.btnAddToCart);
            this.panel1.Controls.Add(this.lblPriceinfo);
            this.panel1.Location = new System.Drawing.Point(12, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(297, 279);
            this.panel1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "₺";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Location = new System.Drawing.Point(528, 121);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(322, 303);
            this.panel2.TabIndex = 15;
            // 
            // UserControlProductInformations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBoxReturn);
            this.Controls.Add(this.pictureBoxItem);
            this.Name = "UserControlProductInformations";
            this.Size = new System.Drawing.Size(1020, 545);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxReturn)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxItem;
        private System.Windows.Forms.Label lblPriceH;
        private System.Windows.Forms.Label lblNameH;
        private System.Windows.Forms.Label lblDescriptionH;
        private System.Windows.Forms.Button btnAddToCart;
        private System.Windows.Forms.Label lblPriceinfo;
        private System.Windows.Forms.Label lblNameinfo;
        private System.Windows.Forms.Label lblDescriptioninfo;
        private System.Windows.Forms.PictureBox pictureBoxReturn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
    }
}
