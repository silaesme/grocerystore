﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;

namespace GroceryStoreApp
{
    public partial class FormGroceryStoreAdminPanel : Form
    {

        SqlCommand Command;
        SqlDataAdapter DataAdapter;

        public FormGroceryStoreAdminPanel()
        {
            InitializeComponent();
        }

        // Create an array to hold all your objects
        private void CallProduct()
        {
            Database database = Database.CreateSingle();
            database.GetConnection();
            Database.CreateSingle().Sqlconnection.Open();
            DataAdapter = new SqlDataAdapter("SELECT * FROM PRODUCTS", Database.CreateSingle().Sqlconnection);
            DataTable table = new DataTable();
            DataAdapter.Fill(table);
            dataGridViewProducts.DataSource = table;
            Database.CreateSingle().Sqlconnection.Close();
        }
        private void GroceryStoreAdminPanel_Load(object sender, EventArgs e)
        {
            CallProduct();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = false;
            base.OnFormClosing(e);
            Application.Exit();
        }

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtName.Text) && !String.IsNullOrEmpty(txtPrice.Text) && !String.IsNullOrEmpty(txtDescription.Text))
            {
                

                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                DialogResult dgResult = openFileDialog1.ShowDialog();
                pictureBoxImage.ImageLocation = openFileDialog1.FileName;
                if (dgResult == DialogResult.OK)
                {
                    string filename = Path.GetFileName(openFileDialog1.FileName);
                    string destinationPath = Path.Combine(Environment.CurrentDirectory, @"Images", filename);

                    if (openFileDialog1.FileName != destinationPath)
                        System.IO.File.Copy(openFileDialog1.FileName, destinationPath);

                    string command = "INSERT INTO PRODUCTS(Name,Description,Price, Path) VALUES(@Name, @Description, @Price, @Path)";
                    Command = new SqlCommand(command, Database.CreateSingle().Sqlconnection);
                    Command.Parameters.AddWithValue("@Name", txtName.Text);
                    Command.Parameters.AddWithValue("@Description", txtDescription.Text);
                    Command.Parameters.AddWithValue("@Price", txtPrice.Text);
                    Command.Parameters.AddWithValue("@Path", filename);

                    Database.CreateSingle().Sqlconnection.Open();
                    Command.ExecuteNonQuery();
                    Database.CreateSingle().Sqlconnection.Close();
                }

            } 
            CallProduct();
            txtName.Text = "";
            txtPrice.Text = "";
            txtDescription.Text = "";
        }

        

        private void Update_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dataGridViewProducts.CurrentRow.Cells[3].Value);

            
            string command = "UPDATE PRODUCTS SET Name = @Name, Description = @Description, Price = @Price Where ID='" + id.ToString() + "'";

            Command = new SqlCommand(command, Database.CreateSingle().Sqlconnection);
            Command.Parameters.AddWithValue("@Name", txtName.Text);
            Command.Parameters.AddWithValue("@Description", txtDescription.Text);
            Command.Parameters.AddWithValue("@Price", txtPrice.Text);

            Database.CreateSingle().Sqlconnection.Open();
            Command.ExecuteNonQuery();
            Database.CreateSingle().Sqlconnection.Close();
            CallProduct();
            
        }

        private void btnUpdateImg_Click(object sender, EventArgs e)
        {
            bool flag = false;
            OpenFileDialog openFileDialog2 = new OpenFileDialog();
            DialogResult dgResult = openFileDialog2.ShowDialog();
            pictureBoxImage.ImageLocation = openFileDialog2.FileName;
            if (dgResult == DialogResult.OK)
            {
                string filename = Path.GetFileName(openFileDialog2.FileName);
                string destinationPath = Path.Combine(Environment.CurrentDirectory, @"Images", filename);

                var dirs = Directory.GetFiles(@"Images", "*.*").Where(s => s.EndsWith(".png") || s.EndsWith(".jpg") || s.EndsWith(".jpeg"));

                foreach (var item in dirs)
                {
                    if (Path.GetFileName(item) == openFileDialog2.SafeFileName)
                    {
                        flag = true;
                    }
                }

                if (flag == false)
                {
                    if (openFileDialog2.FileName != destinationPath)
                        System.IO.File.Copy(openFileDialog2.FileName, destinationPath);
                }

                int secilen = dataGridViewProducts.SelectedCells[0].RowIndex;
                Database.CreateSingle().Sqlconnection.Open();
                SqlCommand command = new SqlCommand("UPDATE dbo.Products SET Path=@filename where ID=@id", Database.CreateSingle().Sqlconnection);
                command.Parameters.AddWithValue("@filename", filename);
                command.Parameters.AddWithValue("@id", dataGridViewProducts.Rows[secilen].Cells[3].Value);

                command.ExecuteNonQuery();
                Database.CreateSingle().Sqlconnection.Close();
                MessageBox.Show("The image is updated.");
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dataGridViewProducts.CurrentRow.Cells[3].Value);

            string command = "DELETE FROM PRODUCTS WHERE  ID='" + id.ToString() + "'";
            Command = new SqlCommand(command, Database.CreateSingle().Sqlconnection);

            Database.CreateSingle().Sqlconnection.Open();
            Command.ExecuteNonQuery();
            Database.CreateSingle().Sqlconnection.Close();
            CallProduct();
        }
        private void pictureBoxExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox_back_Click(object sender, EventArgs e)
        {
            FormSelection selection = new();
            selection.Show();
            this.Hide();
        }

        private void dataGridViewProducts_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtName.Text = dataGridViewProducts.CurrentRow.Cells[0].Value.ToString();
            txtDescription.Text = dataGridViewProducts.CurrentRow.Cells[1].Value.ToString();
            txtPrice.Text = dataGridViewProducts.CurrentRow.Cells[2].Value.ToString();
            //string path = Path.Combine(Environment.CurrentDirectory, @"Images\", fileName);
            //image = Image.FromFile(path);
        }


        private void dataGridViewProducts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilen = dataGridViewProducts.SelectedCells[0].RowIndex;
            string imagename = "";
            var dirs = Directory.GetFiles(@"Images", "*.*").Where(s => s.EndsWith(".png") || s.EndsWith(".jpg") || s.EndsWith(".jpeg"));
            SqlCommand command = new SqlCommand("SELECT Path FROM dbo.Products WHERE ID = @id", Database.CreateSingle().Sqlconnection);
            command.Parameters.AddWithValue("@id", dataGridViewProducts.Rows[secilen].Cells[3].Value.ToString());
            Database.CreateSingle().Sqlconnection.Open();
            SqlDataReader dr = command.ExecuteReader();

            while (dr.Read())
            {
                imagename = dr.GetString(0);
            }
            Database.CreateSingle().Sqlconnection.Close();

            foreach (var item in dirs)
            {
                if (Path.GetFileName(item) == imagename)
                {
                    pictureBoxImage.Image = new Bitmap(item);
                }
            }
        }
    }
}